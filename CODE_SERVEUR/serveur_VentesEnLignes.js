var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/VentesEnLignes';
express  = require("express");
const bodyParser= require('body-parser');
var app = express();

app.use(bodyParser.urlencoded({extended: true}))
var fs = require("fs");
var mesFonctions = require("./mesFonctions.js");

/*
Nom de la base : VentesEnLignes
Nom de la collection : Produits
Documents : [{'type': 'téléphone', 'marque':'Apple', 'nom':'iphone'}, {'type': 'téléphone', 'marque':'Samsung', 'nom':'S7'}]
*/

//Connexion à mongo
MongoClient.connect(url, function(err, database) {
  if (err) throw err;
  db = database;
    console.log("je suis au serveur");
  app.listen(9980);
});

//Qaund je recois un appel de composant1 sans filtre avec la fonction rechercheProduits (callback)
app.get("/produits/marque", function(req, res) {
    console.log("je prends que les marques");
    rechercheProduits(db, {}, function(json) {
         //console.log(json);
         res.setHeader('Access-Control-Allow-Origin', '*');
         res.setHeader('Content-Type', 'application/json');
         res.end(JSON.stringify(json));
    });
});

//quand je recois un appel de composant2 sans filtre sans doublons
app.get("/produits/type", function(req, res) {
    console.log("je prends que les types");
    rechercheType(db, {}, function(json) {
        console.log(json);
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(json));
    });
});

//quand je recois un appel de composant4 avec le parametre marque
app.get("/produits/marque/:marque", function(req, res) {
    var marqueARechercher = req.params.marque;
    console.log("c ma marque !!");
    console.log(marqueARechercher);
    console.log("Marque à rechercher : "+marqueARechercher);
    findProduit(db, {"marque":marqueARechercher}, function(json) {
        console.log(json);
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(json));
    });
});

//quand je recois un appel de composant5 avec le parametre marque
app.get("/produits/nom/:nom", function(req, res) {
    var nomARechercher = req.params.nom;
    console.log("c mon nom de produit !!");
    console.log(nomARechercher);
    console.log("Nom à rechercher : "+nomARechercher);
    findProduit(db, {"nom":nomARechercher}, function(json) {
        console.log(json);
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(json));
    });
});

//La fonction caallback pour les deux chemins précédents
var rechercheProduits = function(db, search, callback) {
    var cursor = db.collection('Produits').find( search );
    var res = [];
    var i=0;
    cursor.each(function(err, doc) {
        //console.log(doc);
        assert.equal(err, null);
        console.log(".............doc1");
        console.log(doc);

        if (doc != null) {
            console.log(res.length);
            //var marque = doc['marque'];
            var marque = doc.marque;
            console.log(" ////////////////////////////////////////// marque ");
            console.log(marque);
                console.log("res a la taille de zéro je rajoute le doc alors et j'affiche mon res puis sa taille");
                var marque=doc.marque;
                console.log("marqueeeeeeeeee");
                console.log(marque);
                res.push(marque);
                console.log("..........................resDe1");
                console.log(res);
                console.log(res.length);


            var newRes =mesFonctions.cleanRes(res);
            res = newRes
            console.log(res);

            i++;
            console.log("///////////////////////////////////////////////////////////mon i");
            console.log(i);

        }
        else { callback(res); }
    });
};

//callback du composant2 recherche par type sans doublons
var rechercheType = function(db, search, callback) {
    var cursor = db.collection('Produits').find( search );
    var res = [];
    var i=0;
    cursor.each(function(err, doc) {
        //console.log(doc);
        assert.equal(err, null);
        console.log(".............doc1");
        console.log(doc);

        if (doc != null) {
            console.log(res.length);
            //var marque = doc['marque'];
            var type=doc.type;
            res.push(type);
            var newRes =mesFonctions.cleanRes(res);
            console.log("mon res ");
            res = newRes
            console.log(res);
            i++;
        }
        else { callback(res); }
    });
};

//la fonction qui retourne que les téléphones
app.get("/produits/:nom", function(req, res) {
    console.log("je prends que les types");
    findProduit(db, {"type":"telephone","nom" : "Iphone3"}, function(json) {
        console.log(json);
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(json));
    });
});

var findProduit = function (db, search, callback) {
    var cursor = db.collection('Produits').find(search);
    var res = [];
    cursor.each(function (err, doc) {
        //assert.equal(err, doc);
        assert.equal(err, null);
        if (doc != null){
            console.log("..................doc4");
            console.log(doc);
            res.push(doc);
        }
        else {callback(res);}
        console.log("\n");
    });
};



//Ajout d'un produits
app.post("/produits/ajout/", function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(json));
    var db = db.collection('Produits');
    var res = {};
    db.type = req.body.type;
    db.marque = req.body.marque;
    db.nom = req.body.nom;
    db.ref = req.body.ref;
    db.prix = req.body.prix;
    var db = db.collection('Produits');
  console.log(db);
    db.save(function(err){
        // save() will run insert() command of MongoDB.
        // it will add new data in collection.
        if(err) {
            response = {"error" : true,"message" : "Error adding data"};
        } else {
            response = {"error" : false,"message" : "Data added"};
        }
        res.json(response);

    });

});












